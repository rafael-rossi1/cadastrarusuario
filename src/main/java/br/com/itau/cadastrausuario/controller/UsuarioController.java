package br.com.itau.cadastrausuario.controller;

import br.com.itau.cadastrausuario.models.UsuarioApp;
import br.com.itau.cadastrausuario.security.Usuario;
import br.com.itau.cadastrausuario.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {

    @Autowired
    UsuarioService usuarioService;

        @PostMapping("{email}")
        public UsuarioApp create(@PathVariable String email, @AuthenticationPrincipal Usuario usuario) {
            UsuarioApp usuarioApp = new UsuarioApp();
            usuarioApp.setNome(usuario.getName());
            usuarioApp.setEmail(email);
            usuarioService.create(usuarioApp);
            return usuarioApp;
        }
    }
