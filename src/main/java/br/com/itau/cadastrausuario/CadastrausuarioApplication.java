package br.com.itau.cadastrausuario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CadastrausuarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(CadastrausuarioApplication.class, args);
	}

}
