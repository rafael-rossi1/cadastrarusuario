package br.com.itau.cadastrausuario.repositories;

import br.com.itau.cadastrausuario.models.UsuarioApp;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<UsuarioApp,Integer> {
}
