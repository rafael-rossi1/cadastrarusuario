package br.com.itau.cadastrausuario.services;

import br.com.itau.cadastrausuario.models.UsuarioApp;
import br.com.itau.cadastrausuario.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public UsuarioApp create(UsuarioApp usuario) {
        return usuarioRepository.save(usuario);
    }

    public UsuarioApp getById(int id) {
        Optional<UsuarioApp> byId = usuarioRepository.findById(id);
        return byId.get();
    }
}
